# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANT_ROOT = File.dirname(File.expand_path(__FILE__))

# Require YAML module
require 'yaml'
# require 'getoptlong'
require 'fileutils'
require 'shellwords'

class String
    def black;          "\e[30m#{self}\e[0m" end
    def red;            "\e[31m#{self}\e[0m" end
    def cyan;           "\e[36m#{self}\e[0m" end
end

 if ARGV[0] == "up"
   unless `ps alx | grep [v]boxwebsrv` != ""
     printf "starting virtualbox web server\n"
     print `VBoxManage setproperty websrvauthlibrary null && vboxwebsrv -H 0.0.0.0 --background`
   end
 end

engine_version=''
engine_mode='default'
proxy = ''

config = YAML.load_file(File.join(File.dirname(__FILE__), 'config.yml'))

base_box=config['environment']['base_box']
# if (defined?(config['environment']['base_box_version'])).nil?
  base_box_version=config['environment']['base_box_version']
# else
#   base_box_version="none"
# end

engine_version=config['environment']['engine_version']

swarm_master_ip=config['environment']['swarm_masterip']

domain=config['environment']['domain']

boxes = config['boxes']

boxes_hostsfile_entries=""


## EXPERIMENTAL FEATURES

experimental=config['environment']['experimental']

########

## TLS

tls_enabled = config['environment']['tls_enabled']
tls_passphrase = config['environment']['tls_passphrase']

########

## STORAGE

additional_disk=config['storage']['additional_disk']
additional_disk_mount=config['storage']['additional_disk_mount']

# Additional Disk
$prepare_additional_disk = <<SCRIPT
 sudo mkdir -p #{additional_disk_mount}
 sudo chmod -R 777  #{additional_disk_mount}
 sudo mkfs.xfs -f /dev/sdb
 [ $(grep -c /dev/sdb /etc/fstab) -eq 0 ] && echo "/dev/sdb #{additional_disk_mount} xfs defaults 0 1" |sudo tee -a /etc/fstab
 sudo mount -a
SCRIPT

######## #mount /dev/sdb #{additional_disk_mount}


boxes.each do |box|
  boxes_hostsfile_entries=boxes_hostsfile_entries+box['mgmt_ip'] + ' ' +  box['name'] + ' ' + box['name']+'.'+domain+'\n'
end

#puts boxes_hostsfile_entries

update_hosts = <<SCRIPT
    echo "127.0.0.1 localhost" >/etc/hosts
    echo -e "#{boxes_hostsfile_entries}" |tee -a /etc/hosts
SCRIPT

puts '--------------------------------------------------------------------------------------------'

puts ' BOX '+base_box + ' ' + base_box_version

puts ' Docker SWARM MODE Vagrant Environment'.cyan

puts ' Engine Version: '+engine_version

puts " Experimental Features Enabled" if experimental == true

puts " Engine Secured with TLS (reacheable on 2376 port - vagrant host 5556 if available)" if tls_enabled == true

puts '--------------------------------------------------------------------------------------------'

$install_docker_engine = <<SCRIPT
  #curl -sSk $1 | sh
  sudo yum install -y sudo yum-utils \
  device-mapper-persistent-data \
  lvm2
  sudo yum-config-manager -y \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

  sudo yum install -y $1  
  sudo usermod -aG docker vagrant 2>/dev/null
  sudo systemctl restart docker
SCRIPT

$enable_experimental_features = <<SCRIPT
    echo '{"experimental" : true}'> /etc/docker/daemon.json
    systemctl restart docker
SCRIPT


Vagrant.configure(2) do |config|
  config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"
  if Vagrant.has_plugin?("vagrant-proxyconf")
    if proxy != ''
        puts " Using proxy"
        config.proxy.http = proxy
        config.proxy.https = proxy
        config.proxy.no_proxy = "localhost,127.0.0.1"
    end
  end
  config.vm.box = base_box
  # if base_box_version != "none"
  # 	text="Using "+base_box+" version "+base_box_version
	#   puts text.red
  	config.vm.box_version = base_box_version
  # end
  
  case engine_version
  when "latest"
      engine_package="docker-ce"
  else
      engine_package="docker-ce="+engine_version
  end

  text= "Using engine version "+engine_version
  puts text.red

  config.vm.synced_folder "tmp_deploying_stage/", "/tmp_deploying_stage",create:true
  config.vm.synced_folder "src/", "/src",create:true
  boxes.each do |node|
    config.vm.define node['name'] do |config|
      config.vm.hostname = node['name']
      config.vm.provider "virtualbox" do |v|
        v.customize [ "modifyvm", :id, "--uartmode1", "disconnected" ]        
        v.name = node['name']
        v.customize ["modifyvm", :id, "--memory", node['mem']]
        v.customize ["modifyvm", :id, "--cpus", node['cpu']]
        v.customize ["modifyvm", :id, "--macaddress1", "auto"]
        v.customize ["modifyvm", :id, "--nictype1", "Am79C973"]
        v.customize ["modifyvm", :id, "--nictype2", "Am79C973"]
        v.customize ["modifyvm", :id, "--nictype3", "Am79C973"]
        v.customize ["modifyvm", :id, "--nictype4", "Am79C973"]
        v.customize ["modifyvm", :id, "--nicpromisc1", "allow-all"]
        v.customize ["modifyvm", :id, "--nicpromisc2", "allow-all"]
        v.customize ["modifyvm", :id, "--nicpromisc3", "allow-all"]
        v.customize ["modifyvm", :id, "--nicpromisc4", "allow-all"]
        v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]

        if additional_disk == true
            data_disk_file = File.join(VAGRANT_ROOT, node['name'] + '-additional_disk.vdi')
              unless File.exist?(data_disk_file)
                v.customize ['createhd', '--filename', data_disk_file, '--size', 50 * 1024]
              end
              v.customize ['storageattach', :id, '--storagectl', 'SATA', '--port', 2, '--device', 0, '--type', 'hdd', '--medium', data_disk_file]
              config.vm.provision "shell" do |s|
                s.inline     = $prepare_additional_disk
              end
              
        end   

      end

      config.vm.network "private_network",
      ip: node['mgmt_ip'],:netmask => "255.255.255.0",
      virtualbox__intnet: false,
      hostonlyadapter: ["vboxnet1"]


      config.vm.network "public_network",
      bridge: ["eth0","enp4s0","wlp3s0","enp3s0f1","wlp2s0"],
      auto_config: true

      config.vm.provision "shell", inline: <<-SHELL
        sudo systemctl stop firewalld
        sudo yum install -y ntp curl && sudo timedatectl set-timezone Europe/Madrid
      SHELL

      config.vm.provision :shell, :inline => update_hosts

      ## Docker Install
      #puts " Community Engine downloaded from " + engine_download_url

      config.vm.provision "shell" do |s|
       			s.name       = "Install Docker Engine version "+engine_version
        		s.inline     = $install_docker_engine
            s.args       = engine_package
      end

      if experimental == true
        #puts " Experimental Features Enabled"

        config.vm.provision "shell" do |s|
              s.name       = "Experimental Features Enabled on Engine"
              s.inline     = $enable_experimental_features
        end
      end
    

      
      if tls_enabled == true

        config.vm.network "forwarded_port", guest: 2376, host: 5556, auto_correct: true

        ## Docker Secure Engine with TLS
        #puts " Engine Secured with TLS (reacheable on 2376 port - vagrant host 5556 if available)"

        config.vm.provision "file", source: "create_tls_certs.sh", destination: "/tmp/create_tls_certs.sh"
        config.vm.provision :shell, :path => 'create_tls_certs.sh' , :args => [ tls_passphrase, node['mgmt_ip'], node['hostonly_ip'], node['name']  ]

      end
    
      ## Create Docker Swarm (Swarm Mode)

      config.vm.provision "file", source: "create_swarm.sh", destination: "/tmp/create_swarm.sh"
      config.vm.provision :shell, :path => 'create_swarm.sh' , :args => [ node['mgmt_ip'], node['swarm_role'], swarm_master_ip, engine_mode ]


      ## Install docker-compose

      config.vm.provision "file", source: "install_compose.sh", destination: "/tmp/install_compose.sh"
      config.vm.provision :shell, :path => 'install_compose.sh'

    end
  end

end
